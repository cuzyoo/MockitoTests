package ch.levin.mockitotests;

import static org.mockito.Mockito.*;

import java.awt.List;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.LinkedList;

import org.hamcrest.Description;

import ch.levin.mockitotests.car.Car;
import ch.levin.mockitotests.car.EngineBroken;

public class Example {

	public static void main(String[] args) {
		Example x = new Example();
		// x.firstExpl();
		// x.secExpl();
		x.personExpl();
		//x.carExpl();
	}

	public Example() {
		
	}

	public void carExpl() {
		Car car1 = mock(Car.class);
		when(car1.startMotor()).thenThrow(EngineBroken.class).getMock();

		car1.startMotor();
	}

	public void personExpl() {
		Person person1 = mock(Person.class);
		System.out.println(person1.getFullName());

		person1.setPrename("Levin");
		person1.setSurname("Germann");

		when(person1.getFullName()).thenReturn("Hans Peter");

		System.out.println(person1.getFullName());

		/*
		 * ERROR:
		 */
		verify(person1).setPrename("LEvin");

		verify(person1).setPrename("Levin");
		when(person1.getPrename()).thenReturn("NotLevin");
		System.out.println(person1.getPrename());
	}

	public void secExpl() {
		List mockedList = mock(List.class);

		mockedList.add("one");
		mockedList.clear();

		// verify
		verify(mockedList).add("one");
		verify(mockedList, times(1)).clear();

		/*
		 * times(1) = wie wenn nichts angegeben ist!
		 */
		verify(mockedList).add("two");
	}

	public void firstExpl() {
		// Ein Objekt mit mock() instanzieren
		LinkedList mockedList = mock(LinkedList.class);

		when(mockedList.get(0)).thenReturn("first");
		when(mockedList.get(1)).thenReturn(4);

		// 2x aufrufen
		System.out.println(mockedList.get(0));
		System.out.println(mockedList.get(0));

		System.out.println(mockedList.get(1));

		System.out.println(mockedList.get(999));

		verify(mockedList, atLeast(4)).get(0);
	}

}
