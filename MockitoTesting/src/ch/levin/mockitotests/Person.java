package ch.levin.mockitotests;

public class Person {
	private String prename;
	private String surname;
	
	public Person(String prename, String surname) {
		this.prename = prename;
		this.surname = surname;
	}
	
	public String getFullName(){
		return this.prename + " " + this.surname;
	}

	public String getPrename() {
		return prename;
	}

	public void setPrename(String prename) {
		this.prename = prename;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}
}
