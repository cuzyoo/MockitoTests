package ch.levin.mockitotests.car;

public class Car {
	private String color;
	private boolean motorStarted;

	public Car(String color) {
		super();
		this.color = color;
		this.motorStarted = false;
	}
	
	public boolean startMotor(){
		boolean result;
		
		if(this.motorStarted){
			System.out.println("Motor already started!");
			result = false;
		}else{
			this.motorStarted = true;
			System.out.println("Motor now running!");
			result = true;
		}
		
		return result;
	}
}
