package ch.levin.mockitotests.car;

public class EngineBroken extends Exception{

	public EngineBroken(String message) {
		super("Engine broken: " + message);
	}
}
